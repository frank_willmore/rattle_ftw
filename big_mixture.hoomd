#!/usr/bin/env hoomd

from hoomd_script import *
from hoomd_plugins import hpmc
import numpy as np

import rattle
from rattle import create_rattle

import subprocess

posfile = hpmc.util.read_pos('mixture.pos')
N = len(posfile['positions'])
#system = init.create_empty(N=N, box=posfile['box'])
system = init.create_empty(N=N, box=posfile['box'], particle_types=['A', 'B'])

mc = hpmc.integrate.convex_polyhedron(seed=1)

for i in range(N):
  p = system.particles[i]
  p.position = posfile['positions'][i]
  p.orientation = posfile['orientations'][i]
  p.type = posfile['types'][i]
		
verts_A = posfile['param_dict']['A']['verts']
verts_B = posfile['param_dict']['B']['verts']
mc.shape_param.set('A', vertices=verts_A)
mc.shape_param.set('B', vertices=verts_B)
system.replicate(nx=20,ny=3,nz=20)
#system.replicate(nx=1,ny=3,nz=1)

# resize to make it big enough for all the particles
resize_scale = 1.1
Lx = system.box.Lx
Ly = system.box.Ly
Lz = system.box.Lz
update.box_resize(Lx = resize_scale*system.box.Lx, Ly=resize_scale*system.box.Ly, Lz=resize_scale*system.box.Lz, period=None)

# everything above is to get the simulation going. Below is the callback that gets run.

def create_scene_file(timestep):
  verts_dict = {'A':verts_A, 'B':verts_B}
  r=create_rattle(system, verts_dict)
  r.addRenderType('A', 'tetrahedron')
  r.addRenderType('B', 'truncated_tetrahedron')
  output_file = os.getenv('TMPDIR', '/tmp') + "/Scene-" + str(timestep).zfill(6) + ".pov"
  r.openOutputFile(output_file)
  r.setCamera(np.array([0.0, 200.0, 0.0], dtype=np.float64))
  r.setLookAt(np.array([0,0,0], dtype=np.float64))
  r.setBackgroundColor("<0, 0.153, 0.298>")
  r.setShapeTexture("texture { pigment {color <1.0, 0.796, 0.0196> transmit 0.25}}") 
  r.printSDLHeader()
  r.setResolution(.05)
  for i in range(len(system.particles)):
    r.rattleShape(i)
    r.printRattleSDL(i)
    r.printSingleShapeSDL(i)
  # This will cause frames to be rendered on the fly, in 4320p
  subprocess.call(["povray", "-W7680", "-H4320", output_file])
  r.openOutputFile("/tmp/rattle.rat")
  r.dumpStatistics()

# increase run time to generate more frames
run(30, callback=create_scene_file, callback_period=1);

