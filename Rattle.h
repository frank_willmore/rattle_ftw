// Rattle.h

#include <hoomd/hoomd_config.h>
#include <hoomd/HOOMDMath.h>
#include <HPMCPrecisionSetup.h>
#include <hoomd/VectorMath.h>
#include <ShapeConvexPolyhedron.h>

#include "typed_poly3d_verts.h"

#include <boost/python.hpp>
#include <iostream>
#include <vector>
#include <unordered_map>

#ifndef __RATTLE_H__
#define __RATTLE_H__

#define RATTLE_VERLET_MAX 1024

enum render_t { tetrahedron, truncated_tetrahedron };           ///< enumerated type expressing values of types whose rendering protocol is implemented

class Rattle{

    std::vector<typed_poly3d_verts> v_verts;                    ///< vector of the vertices for given particle type

    std::vector<char> v_particle_type_ids;                      ///< 'A', 'B', etc

    std::vector<vec3<Scalar> > v_positions;                     ///< vector to hold positions of all particles

    std::vector<quat<Scalar> > v_orientations;                  ///< vector to hold orientations of all particles

    std::vector<hpmc::ShapeConvexPolyhedron*> v_scp;            ///< vector to hold ShapeConvexPolyhedron of all particles

    std::unordered_map<char, render_t> v_render_types;          ///< maps particle types ('A', 'B', etc.) to render types (truncated_tetrahedron, tetrahedron, etc.)

    int verlet_indices[RATTLE_VERLET_MAX];                      ///< list of particle numbers included when building the verlet neighbor list for particle_of_interest

    unsigned int index_of_index;                                ///< size of verlet list, used when adding members to list    

		/// table of binary values marking whether a given space has been visited when determining inclusion in rattle volume.
		/// note that search starts at site [128][128][128]
    unsigned int recursion_matrix[256][256][256];               

    Scalar resolution;                                          ///< translation distance between points when sampling space for rattle volume

    Scalar verlet_sq;                                           ///< square of distance to be used when determining whether particles are to be included in verlet list

    unsigned int n_particles;                                   ///< number of particles in system (does not include supercell/replicas)

    Scalar box_x, box_y, box_z;                                 ///< dimensions of simulation box, which is assumed to be tetragonal

    std::vector<std::vector<vec3<Scalar>*>*> *vv_recorded_points; ///< locations of all the recorded points (rattle point cloud)

    FILE *output_file;                                          ///< file pointer for the POVRay(SDL) file

    const char *output_filename;                                ///< name of the POVRay(SDL) file 

    const char *shape_texture;                                  ///< string containing POVRay SDL texture descriptor to be used when rendering faces of the particle shape

    const char *rattle_texture;                                 ///< string containing POVRay SDL texture descriptor to be used when rendering surfaces of the rattle volume 

    const char *background_color;                               ///< Background color for SDL file, could be either a known color e.g. "Red" or a color tuple e.g. "<1, 0, 0>"

    unsigned int configuration_is_finalized;                    ///< truth test for whether finalizeConfiguration() has been run (which causes mirror boxes to be applied)

    unsigned int use_fill_light;                                ///< Adds fill light to scene file. Useful when e.g., direct light gives glare

    float camera_x, camera_y, camera_z;                         ///< location of camera in SDL file

    float look_at_x, look_at_y, look_at_z;                      ///< point at which camera is pointed in SDL file

    // Methods

    void visit(unsigned int shape_of_interest, int _i, int _j, int _k); ///< recursive routine which determines discrete continuity of rattle volume

    void buildVerletList(unsigned int shape_of_interest);       ///< builds a verlet list for the current shape of interest


  public:

    /// Construct a Rattle object which will be valid for and representative of a single simulation frame
    Rattle(boost::python::numeric::array _box_dim, ///<      - dimensions of tetragonal simulation box
		       int _n_particles,                       ///<      - number of particles in system
					 int _n_particle_types);                 ///<      - number of distinct types of particles to be specified to the system

    ~Rattle();

		/// maps a particle_type_id (e.g. A) to a specific render_type (e.g. truncated_tetrahedron) which Rattle::Rattle knows how to draw.
		/// The rattle volume can still be calculated for objects of type ShapeConvexPolyhedron, but only objects of a known render_type can be drawn.
    void addRenderType(char _particle_type_id,     ///<      - type_id for the particle, e.g. 'A', 'B', etc.
		                   char *_render_type);        ///<      - maps to the enumerated type render_t

		/// add a particle_type_id (e.g. 'A', 'B', etc) from the simulation and specify the number of vertices for it.
    void addParticleTypeID(char _particle_type_id, ///<      - type_id for the particle, e.g. 'A', 'B', etc.
		                       int _n_verts);          ///<      - number of vertices for this type 

	  /// add one of the vertices for the given type_id ('A', 'B', etc.).  Must be specified one at a time by calling this method.
    void addVertex(char _particle_type_id,                  ///<      - 'A', 'B', etc. 
		               boost::python::numeric::array _vertex);  ///<      - 3-tuple of the coordinates for the vertex

    /// add a particle from the simulation to the Rattle object. Particles must be added one at a time to the Rattle object.
    void addParticle(char _particle_type_id,                      ///<      - 'A', 'B', etc.
		                 boost::python::numeric::array _position,     ///<      - 3-tuple of the (X,Y,Z) position of this particle
										 boost::python::numeric::array _orientation); ///<      - 4-tuple of the (cos(theta/2), Ux, Uy, Uz) rotation/orientation quaternion of this particle

    /// set the location of the camera in the scene file for rendering
    void setCamera(boost::python::numeric::array _camera);  ///<     - 3-tuple of the (X,Y,Z) position of the camera

    /// set the location toward which the camera is pointed in the scene file for rendering
    void setLookAt(boost::python::numeric::array _look_at); ///<     - 3-tuple of the (X,Y,Z) position toward which the camera is pointed

    /// generate super-cell of original simulation box by applying replicas of atoms in original tetragon box for one half of box length in each of the three cardinal directions.
		/// First n particles are original shapes;  7 boxes (X, Y, Z, XY, XZ, YZ, XYZ diretions) added to store replicas in v_positions.
    void finalizeConfiguration();

		/// generate rattle information for the indicated shape (should be one of original shapes specified as input, not a replica.)
    void rattleShape(unsigned int shape_of_interest);

		/// print the POVRay Scene Definition Language for all shapes
    void printRattleSDL(unsigned int shape_of_interest);

		/// print the POVRay Scene Definition Language for the given shape
    void printSingleShapeSDL(unsigned int shape_of_interest);

		/// print the POVRay Scene Definition Language header 
    void printSDLHeader();

		/// set the resolution to which rattle volumes are calculated. Smaller means finer grained, but more time to calculate and render, and risks out-of-bounds and stack overflow
    void setResolution(Scalar _resolution = 0.1);

		/// How large of a radius to be included when specifying Verlet neighbor list
    void setVerletRadius(Scalar _verlet = 4.0);

		/// Open an output file to receive the POVRay SDL generated.
    void openOutputFile(const char* _output_filename);

		/// Specify a POVRay texture as a string to be used for the faces of the shapes drawn.
    void setShapeTexture(const char* _shape_texture);

		/// Specify a POVRay texture for the surfaces of the rattle volumes.
    void setRattleTexture(const char* _rattle_texture);

		/// Specify a background color to be used in the POVRay scene file. Could be a name (Red, Orange, etc.) or a color tuple like <1, 0, 0>, <0.3, 0.4, 0.5>
    void setBackgroundColor(const char* _background_color);

		/// Specify 1 (true) or 0 (false) whether or not to use the fill lights in the POVRay scene file.
    void useFillLight(unsigned int use_fill_light);

		/// Call this function to dump the list of rattle volumes to the file '/tmp/rattle.rat'
    void dumpStatistics();

}; // end class Rattle

#endif // __RATTLE_H__

