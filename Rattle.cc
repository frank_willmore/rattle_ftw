// Rattle.cc

#include <stdio.h>
#include <vector>

#include "Rattle.h"
#include "QuaternionMultiply.h"

/// Helper function, used to generate a POVRay 3-tuple from the given vec3 into the given string buffer 
char *write_tuple(vec3<Scalar> vvv, char *s){
  sprintf(s, "<%g, %g, %g>", vvv.x, vvv.y, vvv.z);
  return s;
}


Rattle::Rattle(boost::python::numeric::array _box_dim, int _n_particles, int _n_particle_types):n_particles(_n_particles){
  
  boost::python::numeric::array box_dim = _box_dim;
  box_x = boost::python::extract<float>(box_dim[0]);
  box_y = boost::python::extract<float>(box_dim[1]);
  box_z = boost::python::extract<float>(box_dim[2]);

  output_filename = "/tmp/Scene.pov";

  //char const *color    = " rgb<1,0.5,0.25> ";
  //char const *transmit = " transmit 0.25 ";
  //char const *finish   = " finish {phong 1} ";
  
  shape_texture = "texture { pigment { color rgb<1.0, 0.5, 0.25> transmit 0.25 } finish {phong 1.0} }";
  rattle_texture = "texture { pigment {color rgb <1, 1, 1>} finish {ambient .8 diffuse .6} }";
  use_fill_light = 1; // use by default
  camera_x = box_x + 1.0;
  camera_y = box_y + 2.0;
  camera_z = box_z + 3.0;
  look_at_x = 0.0;
  look_at_y = 0.0;
  look_at_z = 0.0;
  background_color = "<0.7, 0.8, 0.9>"; // bluish
  output_file = stdout; // default to sending output to stdout

} // end Rattle(...)


void Rattle::addVertex(char _particle_type_id, boost::python::numeric::array _vertex){

printf("addVertex(%c)\n", _particle_type_id);

  boost::python::numeric::array vertex = _vertex;
  float vertex_x = boost::python::extract<float>(vertex[0]);
  float vertex_y = boost::python::extract<float>(vertex[1]);
  float vertex_z = boost::python::extract<float>(vertex[2]);

  // find the right verts object... 
  typed_poly3d_verts *p_verts = NULL;
  for (int i=0; i<v_verts.size(); i++) if (_particle_type_id == v_verts.at(i).particle_type_id) p_verts = &(v_verts.at(i));
  if (p_verts == NULL) printf("particle type '%c' not found.\n", _particle_type_id);
  
  p_verts->x[p_verts->n_recorded_verts] = vertex_x;
  p_verts->y[p_verts->n_recorded_verts] = vertex_y;
  p_verts->z[p_verts->n_recorded_verts] = vertex_z;
  p_verts->n_recorded_verts++;
  if (p_verts->n_recorded_verts > p_verts->N) fprintf(stderr, "Warning:  Adding more vertices than expected. %d/%d\n", p_verts->n_recorded_verts, p_verts->N);

}


void Rattle::addParticleTypeID(char _particle_type_id, int _n_verts){
  printf("addParticleTypeID(%c)\n", _particle_type_id);

  // push new verts object ont v_verts
  v_verts.push_back(typed_poly3d_verts(_particle_type_id));
  v_verts.back().N = _n_verts;
  v_verts.back().n_recorded_verts = 0;

}


void Rattle::addParticle(char _particle_type_id, boost::python::numeric::array _position, boost::python::numeric::array _orientation){

  boost::python::numeric::array position = _position;
  float x = boost::python::extract<float>(position[0]);
  float y = boost::python::extract<float>(position[1]);
  float z = boost::python::extract<float>(position[2]);
  v_positions.push_back(vec3<float>(x,y,z));

  boost::python::numeric::array orientation = _orientation;
  float q_s = boost::python::extract<float>(orientation[0]);
  float q_x = boost::python::extract<float>(orientation[1]);
  float q_y = boost::python::extract<float>(orientation[2]);
  float q_z = boost::python::extract<float>(orientation[3]);
  v_orientations.push_back(quat<float>(q_s, vec3<float>(q_x, q_y, q_z)));

  // Record the shape id. E.g. 'A' could be one sort of tetrahedron, 'B' could be another. 
  v_particle_type_ids.push_back(_particle_type_id);

  if (v_positions.size() > n_particles) fprintf(stderr, "Warning:  Adding more shapes than expected. %lu/%u\n", v_positions.size(), n_particles);

}


void Rattle::finalizeConfiguration(){
  // process positions and orientations recieved from python
  // First n_particles is original shape, wil add 7 boxes (X, Y, Z, XY, XZ, YZ, XYZ) to apply periodic boundary and store replicas in v_positions.
  
  //  if (n_recorded_verts < m_verts.N) {
  //    fprintf(stderr, "Cannot finalize configuration, there are only %d/%d vertices recorded.\n", n_recorded_verts, m_verts.N);
  //    return;
  //  }

  // Apply X half-box replicas
  for (unsigned int i=0; i<n_particles; i++){
    vec3<Scalar> mirror_position = v_positions[i];
    if (mirror_position.x < 0) mirror_position.x += box_x; else mirror_position.x -= box_x; 
    v_positions.push_back(mirror_position);
    v_orientations.push_back(v_orientations[i]);
    v_particle_type_ids.push_back(v_particle_type_ids[i]);
  }

  // Apply Y half-box replicas
  for (unsigned int i=0; i<n_particles; i++){
    vec3<Scalar> mirror_position = v_positions[i];
    if (mirror_position.y < 0) mirror_position.y += box_y; else mirror_position.y -= box_y; 
    v_positions.push_back(mirror_position);
    v_orientations.push_back(v_orientations[i]);
    v_particle_type_ids.push_back(v_particle_type_ids[i]);
  }

  // Apply Z half-box replicas
  for (unsigned int i=0; i<n_particles; i++){
    vec3<Scalar> mirror_position = v_positions[i];
    if (mirror_position.z < 0) mirror_position.z += box_z; else mirror_position.z -= box_z; 
    v_positions.push_back(mirror_position);
    v_orientations.push_back(v_orientations[i]);
    v_particle_type_ids.push_back(v_particle_type_ids[i]);
  }

  // Apply XY quarter-box replicas
  for (unsigned int i=0; i<n_particles; i++){
    vec3<Scalar> mirror_position = v_positions[i];
    if (mirror_position.x < 0) mirror_position.x += box_x; else mirror_position.x -= box_x; 
    if (mirror_position.y < 0) mirror_position.y += box_y; else mirror_position.y -= box_y; 
    v_positions.push_back(mirror_position);
    v_orientations.push_back(v_orientations[i]);
    v_particle_type_ids.push_back(v_particle_type_ids[i]);
  }

  // Apply XZ quarter-box replicas
  for (unsigned int i=0; i<n_particles; i++){
    vec3<Scalar> mirror_position = v_positions[i];
    if (mirror_position.x < 0) mirror_position.x += box_x; else mirror_position.x -= box_x; 
    if (mirror_position.z < 0) mirror_position.z += box_z; else mirror_position.z -= box_z; 
    v_positions.push_back(mirror_position);
    v_orientations.push_back(v_orientations[i]);
    v_particle_type_ids.push_back(v_particle_type_ids[i]);
  }

  // Apply YZ quarter-box replicas
  for (unsigned int i=0; i<n_particles; i++){
    vec3<Scalar> mirror_position = v_positions[i];
    if (mirror_position.y < 0) mirror_position.y += box_y; else mirror_position.y -= box_y; 
    if (mirror_position.z < 0) mirror_position.z += box_z; else mirror_position.z -= box_z; 
    v_positions.push_back(mirror_position);
    v_orientations.push_back(v_orientations[i]);
    v_particle_type_ids.push_back(v_particle_type_ids[i]);
  }

  // Apply XYZ eighth-box replicas
  for (unsigned int i=0; i<n_particles; i++){
    vec3<Scalar> mirror_position = v_positions[i];
    if (mirror_position.x < 0) mirror_position.x += box_x; else mirror_position.x -= box_x; 
    if (mirror_position.y < 0) mirror_position.y += box_y; else mirror_position.y -= box_y; 
    if (mirror_position.z < 0) mirror_position.z += box_z; else mirror_position.z -= box_z; 
    v_positions.push_back(mirror_position);
    v_orientations.push_back(v_orientations[i]);
    v_particle_type_ids.push_back(v_particle_type_ids[i]);
  }

  // This will do all the SCP's
  //for (unsigned int i=0; i<8*n_particles; i++) v_scp.push_back(new hpmc::ShapeConvexPolyhedron(v_orientations[i], v_verts[i]));
  for (unsigned int i=0; i<8*n_particles; i++) {
  
    // grab the index and use it to get the right set of verts
    char _particle_type_id = v_particle_type_ids[i];
    typed_poly3d_verts *p_verts = NULL;
    for (int i_verts=0; i_verts<v_verts.size(); i_verts++) if (_particle_type_id == v_verts.at(i_verts).particle_type_id) p_verts = &(v_verts.at(i_verts));
    if (p_verts == NULL) printf("particle type '%c' not found.\n", _particle_type_id);
  
    v_scp.push_back(new hpmc::ShapeConvexPolyhedron(v_orientations[i], *p_verts));
  }

  // now set a few other details
  // vv_recorded_points will store the list of rattled points
  vv_recorded_points = new std::vector<std::vector<vec3<Scalar>*>*>();
  for (unsigned int i=0; i<n_particles; i++) vv_recorded_points->push_back(new std::vector<vec3<Scalar>*>()); // initial points lists with empty vectors;

  setResolution();
  setVerletRadius();

  configuration_is_finalized = 1;

} // finalizeConfiguration()


void Rattle::openOutputFile(const char* _output_filename){
  output_filename = _output_filename;
  printf("opening %s\n", output_filename);
  if (output_file != stdout) fclose(output_file);
  if (output_filename == NULL) output_file = stdout;
  else output_file = fopen(output_filename, "w");
}


Rattle::~Rattle(){
  //std::cout << "destructing Rattle " << endl;
  if (output_file) fclose(output_file);
  // need to delete the objects in all them vectors....
}


void Rattle::setBackgroundColor(const char* _background_color){
  background_color = _background_color;
}


void Rattle::useFillLight(unsigned int _use_fill_light){
  use_fill_light = _use_fill_light;
}


void Rattle::setCamera(boost::python::numeric::array _camera){

  boost::python::numeric::array camera = _camera;
  camera_x = boost::python::extract<float>(camera[0]);
  camera_y = boost::python::extract<float>(camera[1]);
  camera_z = boost::python::extract<float>(camera[2]);

}


void Rattle::setLookAt(boost::python::numeric::array _look_at){

  boost::python::numeric::array look_at = _look_at;
  look_at_x = boost::python::extract<float>(look_at[0]);
  look_at_y = boost::python::extract<float>(look_at[1]);
  look_at_z = boost::python::extract<float>(look_at[2]);

}


void Rattle::setShapeTexture(const char* _shape_texture){

  shape_texture = _shape_texture;

}


void Rattle::setRattleTexture(const char* _rattle_texture){

  rattle_texture = _rattle_texture;

}


void Rattle::printSingleShapeSDL(unsigned int shape_of_interest){

  //  char const *color    = " rgb<1,0.5,0.25> ";
  //  char const *transmit = " transmit 0.7 ";
  //  char const *finish   = " finish {phong 1} ";

  // get the render_type for this particle:
  char particle_type_id = v_particle_type_ids[shape_of_interest];
  render_t particle_render_type = v_render_types[particle_type_id];

  // get the right verts object for this particle
  typed_poly3d_verts *p_verts;
  for (int i=0; i<v_verts.size(); i++) if (particle_type_id == v_verts.at(i).particle_type_id) p_verts = &(v_verts.at(i));
  typed_poly3d_verts verts = *p_verts;
  
  switch(particle_render_type){

    case tetrahedron: {

      vec3<Scalar> v0 = applyRotation(vec3<Scalar>(verts.x[0], verts.y[0], verts.z[0]), v_orientations[shape_of_interest]);
      vec3<Scalar> v1 = applyRotation(vec3<Scalar>(verts.x[1], verts.y[1], verts.z[1]), v_orientations[shape_of_interest]);
      vec3<Scalar> v2 = applyRotation(vec3<Scalar>(verts.x[2], verts.y[2], verts.z[2]), v_orientations[shape_of_interest]);
      vec3<Scalar> v3 = applyRotation(vec3<Scalar>(verts.x[3], verts.y[3], verts.z[3]), v_orientations[shape_of_interest]);

      // add the positions... 
      v0 = v0 + v_positions[shape_of_interest];
      v1 = v1 + v_positions[shape_of_interest];
      v2 = v2 + v_positions[shape_of_interest];
      v3 = v3 + v_positions[shape_of_interest];

      // four vertices, taken three at a time, will give me triangles which can then be used to generate pov info.
      char s1[256],s2[256],s3[256]; // string buffers for use by write_tuple. Need three, otherwise single buffer is overwritten before print finishes.

      fprintf(output_file, "triangle { %s, %s, %s texture {shape_texture} }\n", write_tuple(v0, s1), write_tuple(v1, s2), write_tuple(v2, s3));
      fprintf(output_file, "triangle { %s, %s, %s texture {shape_texture} }\n", write_tuple(v0, s1), write_tuple(v1, s2), write_tuple(v3, s3));
      fprintf(output_file, "triangle { %s, %s, %s texture {shape_texture} }\n", write_tuple(v0, s1), write_tuple(v2, s2), write_tuple(v3, s3));
      fprintf(output_file, "triangle { %s, %s, %s texture {shape_texture} }\n", write_tuple(v1, s1), write_tuple(v2, s2), write_tuple(v3, s3));

      break;

    }
      
    case truncated_tetrahedron: {

      vec3<Scalar> P11 = v_positions[shape_of_interest] + applyRotation(vec3<Scalar>(verts.x[0], verts.y[0], verts.z[0]), v_orientations[shape_of_interest]);
      vec3<Scalar> P12 = v_positions[shape_of_interest] + applyRotation(vec3<Scalar>(verts.x[1], verts.y[1], verts.z[1]), v_orientations[shape_of_interest]);
      vec3<Scalar> P13 = v_positions[shape_of_interest] + applyRotation(vec3<Scalar>(verts.x[2], verts.y[2], verts.z[2]), v_orientations[shape_of_interest]);
      vec3<Scalar> P21 = v_positions[shape_of_interest] + applyRotation(vec3<Scalar>(verts.x[3], verts.y[3], verts.z[3]), v_orientations[shape_of_interest]);
      vec3<Scalar> P22 = v_positions[shape_of_interest] + applyRotation(vec3<Scalar>(verts.x[4], verts.y[4], verts.z[4]), v_orientations[shape_of_interest]);
      vec3<Scalar> P23 = v_positions[shape_of_interest] + applyRotation(vec3<Scalar>(verts.x[5], verts.y[5], verts.z[5]), v_orientations[shape_of_interest]);
      vec3<Scalar> P31 = v_positions[shape_of_interest] + applyRotation(vec3<Scalar>(verts.x[6], verts.y[6], verts.z[6]), v_orientations[shape_of_interest]);
      vec3<Scalar> P32 = v_positions[shape_of_interest] + applyRotation(vec3<Scalar>(verts.x[7], verts.y[7], verts.z[7]), v_orientations[shape_of_interest]);
      vec3<Scalar> P33 = v_positions[shape_of_interest] + applyRotation(vec3<Scalar>(verts.x[8], verts.y[8], verts.z[8]), v_orientations[shape_of_interest]);
      vec3<Scalar> P41 = v_positions[shape_of_interest] + applyRotation(vec3<Scalar>(verts.x[9], verts.y[9], verts.z[9]), v_orientations[shape_of_interest]);
      vec3<Scalar> P42 = v_positions[shape_of_interest] + applyRotation(vec3<Scalar>(verts.x[10], verts.y[10], verts.z[10]), v_orientations[shape_of_interest]);
      vec3<Scalar> P43 = v_positions[shape_of_interest] + applyRotation(vec3<Scalar>(verts.x[11], verts.y[11], verts.z[11]), v_orientations[shape_of_interest]);

      fprintf(output_file, "#declare P11 = <%f, %f, %f>;\n", P11.x, P11.y, P11.z);
      fprintf(output_file, "#declare P12 = <%f, %f, %f>;\n", P12.x, P12.y, P12.z);
      fprintf(output_file, "#declare P13 = <%f, %f, %f>;\n", P13.x, P13.y, P13.z);

      fprintf(output_file, "#declare P21 = <%f, %f, %f>;\n", P21.x, P21.y, P21.z);
      fprintf(output_file, "#declare P22 = <%f, %f, %f>;\n", P22.x, P22.y, P22.z);
      fprintf(output_file, "#declare P23 = <%f, %f, %f>;\n", P23.x, P23.y, P23.z);

      fprintf(output_file, "#declare P31 = <%f, %f, %f>;\n", P31.x, P31.y, P31.z);
      fprintf(output_file, "#declare P32 = <%f, %f, %f>;\n", P32.x, P32.y, P32.z);
      fprintf(output_file, "#declare P33 = <%f, %f, %f>;\n", P33.x, P33.y, P33.z);

      fprintf(output_file, "#declare P41 = <%f, %f, %f>;\n", P41.x, P41.y, P41.z);
      fprintf(output_file, "#declare P42 = <%f, %f, %f>;\n", P42.x, P42.y, P42.z);
      fprintf(output_file, "#declare P43 = <%f, %f, %f>;\n", P43.x, P43.y, P43.z);

      fprintf(output_file, "triangle { P11, P12, P13 texture{ shape_texture } }\n");
      fprintf(output_file, "triangle { P21, P22, P23 texture{ shape_texture } }\n");
      fprintf(output_file, "triangle { P31, P32, P33 texture{ shape_texture } }\n");
      fprintf(output_file, "triangle { P41, P42, P43 texture{ shape_texture } }\n");

      // fprintf(output_file, "polygon{6, P33, P31, P21, P22, P42, P43  pigment{color Orange}}\n");
      fprintf(output_file, "triangle {P33, P31, P21 texture { shape_texture } }\n");
      fprintf(output_file, "triangle {P21, P22, P42 texture { shape_texture } }\n");
      fprintf(output_file, "triangle {P42, P43, P33 texture { shape_texture } }\n");
      fprintf(output_file, "triangle {P33, P21, P42 texture { shape_texture } }\n");
      
      // fprintf(output_file, "polygon{6, P11, P12, P32, P33, P43, P41  pigment{color Orange}}\n");
      fprintf(output_file, "triangle {P11, P12, P32 texture { shape_texture } }\n");
      fprintf(output_file, "triangle {P32, P33, P43 texture { shape_texture } }\n");
      fprintf(output_file, "triangle {P43, P41, P11 texture { shape_texture } }\n");
      fprintf(output_file, "triangle {P11, P32, P43 texture { shape_texture } }\n");
      
      // fprintf(output_file, "polygon{6, P11, P13, P23, P22, P42, P41  pigment{color Orange}}\n");
      fprintf(output_file, "triangle {P11, P13, P23 texture { shape_texture } }\n");
      fprintf(output_file, "triangle {P23, P22, P42 texture { shape_texture } }\n");
      fprintf(output_file, "triangle {P42, P41, P11 texture { shape_texture } }\n");
      fprintf(output_file, "triangle {P11, P23, P42 texture { shape_texture } }\n");
      
      // fprintf(output_file, "polygon{6, P12, P13, P23, P21, P31, P32  pigment{color Orange}}\n");
      fprintf(output_file, "triangle {P12, P13, P23 texture { shape_texture } }\n");
      fprintf(output_file, "triangle {P23, P21, P31 texture { shape_texture } }\n");
      fprintf(output_file, "triangle {P31, P32, P12 texture { shape_texture } }\n");
      fprintf(output_file, "triangle {P12, P23, P31 texture { shape_texture } }\n");

      break;

    } // end case truncated_tetrahedron
      
  } // end switch(particle_render_type)

  fflush(stdout);
  fflush(output_file);

} // printSingleShapeSDL()


void Rattle::setResolution(Scalar _resolution){
  resolution = _resolution;
} // setResolution()


void Rattle::setVerletRadius(Scalar _verlet){
  verlet_sq = _verlet * _verlet;
} // setVerletRadius()


void Rattle::buildVerletList(unsigned int shape_of_interest){

  // clear old Verlet lists
  memset(verlet_indices, 0, sizeof(verlet_indices));
  index_of_index = 0; // to add members to list

  // Notes on Verlet list and periodic boundary: 
  //   - the position list includes locations of periodic images
  //   - the shape list does not!
  //   - the Verlet shape list may have redundant pointers because of different images
  //   - the Verlet position list will not.
  
  for (unsigned int i=0; i<8*n_particles; i++) { // build a verlet list around shape of interest
    if (i==shape_of_interest) continue; // cos we don't count ourself

    Scalar dx = v_positions[i].x - v_positions[shape_of_interest].x; 
    Scalar dy = v_positions[i].y - v_positions[shape_of_interest].y; 
    Scalar dz = v_positions[i].z - v_positions[shape_of_interest].z; 
    if ((dx*dx + dy*dy + dz*dz) < verlet_sq) {
      verlet_indices[index_of_index++] = i; // add the shape to the list.
    }

  } // end loop over n_particles;  done building Verlet list

}


void Rattle::rattleShape(unsigned int shape_of_interest){

  buildVerletList(shape_of_interest);

  // clear recursion_matrix, use 
  memset(recursion_matrix, 0, sizeof(recursion_matrix));

  // initiate recursive search of space
  visit(shape_of_interest, 128, 128, 128);

} // end rattleShape()


void Rattle::visit(unsigned int shape_of_interest, int _i, int _j, int _k) {

  if ((int)_i < 0 || (int)_j < 0 || (int)_k < 0) {fprintf(stderr, "out of bounds at %d::%d::%d\n", _i, _j, _k); return;}
  if ((int)_i > 255 || (int)_j > 255 || (int)_k > 255) {fprintf(stderr, "out of bounds at %d::%d::%d\n", _i, _j, _k); return;}

  if (recursion_matrix[_i][_j][_k]) return; // already  visited
  recursion_matrix[_i][_j][_k] = 1; // mark the visited point

  hpmc::ShapeConvexPolyhedron scp_of_interest = *v_scp[shape_of_interest]; // retrieve the shape object of interest

  // stroll through Verlet list
  for (unsigned int i_verlet=0; i_verlet<index_of_index; i_verlet++) {

    hpmc::ShapeConvexPolyhedron scp = *v_scp[verlet_indices[i_verlet]]; // retrieve the shape object i_shape
    vec3<Scalar> r_ab; 
    r_ab.x = v_positions[shape_of_interest].x - v_positions[verlet_indices[i_verlet]].x + (_i - 128) * resolution;
    r_ab.y = v_positions[shape_of_interest].y - v_positions[verlet_indices[i_verlet]].y + (_j - 128) * resolution;
    r_ab.z = v_positions[shape_of_interest].z - v_positions[verlet_indices[i_verlet]].z + (_k - 128) * resolution;
    unsigned int err;
    
    int overlap=hpmc::test_overlap<hpmc::ShapeConvexPolyhedron,hpmc::ShapeConvexPolyhedron>(r_ab, scp, scp_of_interest, err); 
    if (overlap) return;

  } // end for i_shape loop over verlet

  // record it
  (vv_recorded_points->at(shape_of_interest))->push_back(new vec3<Scalar>(
    v_positions[shape_of_interest].x + (_i - 128) * resolution, 
    v_positions[shape_of_interest].y + (_j - 128) * resolution, 
    v_positions[shape_of_interest].z + (_k - 128) * resolution
  ));

  // visit each neighbor
  visit(shape_of_interest, _i - 1, _j, _k); visit(shape_of_interest, _i + 1, _j, _k);
  visit(shape_of_interest, _i, _j - 1, _k); visit(shape_of_interest, _i, _j + 1, _k);
  visit(shape_of_interest, _i, _j, _k - 1); visit(shape_of_interest, _i, _j, _k + 1);

} // visit()


void Rattle::printRattleSDL(unsigned int shape_of_interest){

  std::vector<vec3<Scalar>*> *v_shape_of_interest = vv_recorded_points->at(shape_of_interest); 

  fprintf(output_file, "blob { threshold 0.650000 \n"); // open the blob...
  
  for (vector<vec3<Scalar>*>::iterator it = v_shape_of_interest->begin(); it != v_shape_of_interest->end(); ++it){
    vec3<Scalar> vo = **it;
    fprintf(output_file, "sphere {<%f,%f,%f>, %f, 1.000000 } \n", vo.x, vo.y, vo.z, resolution*1.7);
  }

  fprintf(output_file, "texture { rattle_texture } } // end blob } // end looks_like } // end light_source\n");

  fflush(stdout);
  fflush(output_file);

} // printRattleSDL()


void Rattle::dumpStatistics(){
  printf("dumping statistics...i\n");
  for (int shape_of_interest=0; shape_of_interest<n_particles; shape_of_interest++) {
    float volume = resolution * resolution * resolution * vv_recorded_points->at(shape_of_interest)->size();
    fprintf(output_file, "%f\n", volume);
  }
}


void Rattle::printSDLHeader() {

    fprintf(output_file, "#include \"colors.inc\"\n");
    fprintf(output_file, "background {color %s}\n", background_color);
    fprintf(output_file, "\n");
    fprintf(output_file, "#declare shape_texture = %s\n", shape_texture);
    fprintf(output_file, "#declare rattle_texture = %s\n", rattle_texture);
    fprintf(output_file, "\n");
    fprintf(output_file, "camera{location<%f, %f, %f> look_at <0,0,0>}\n", camera_x, camera_y, camera_z);
    fprintf(output_file, "global_settings { ambient_light rgb<1, 1, 1> }\n");
    fprintf(output_file, "\n");

    if (use_fill_light){
      fprintf(output_file, "light_source{<100,100,10> color Gray50 shadowless}\n");
      fprintf(output_file, "light_source{<0,100,0> color Gray50 shadowless}\n");
      fprintf(output_file, "light_source{<0,0,100> color Gray50 shadowless}\n");
      fprintf(output_file, "light_source{<-100,0,0> color Gray50 shadowless}\n");
      fprintf(output_file, "light_source{<0,-100,0> color Gray50 shadowless}\n");
      fprintf(output_file, "light_source{<0,0,-100> color Gray50 shadowless}\n");
      fprintf(output_file, "\n");
    }

} // printSDLHeader()


void Rattle::addRenderType(char _type_id, char* _render_type){

  printf("Got id '%c' for render type %s\n", _type_id, _render_type);
  render_t render_type;
  if (strcmp("tetrahedron", _render_type) == 0) render_type = tetrahedron;
  else if (strcmp("truncated_tetrahedron", _render_type) == 0) render_type = truncated_tetrahedron;
  else printf("Unrecognized render type %s\n", _render_type);

  //v_render_types.insert(std::make_pair<char, std::string>(_type_id, std::string(_shape_type))); // move insertion
  //v_render_types.insert(std::make_pair(_type_id, std::string(_shape_type))); // move insertion
  v_render_types.insert(std::make_pair(_type_id, render_type)); // move insertion

}

