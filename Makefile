# location of the Python header files
 
HPMC_INCLUDE=/home/frankted/opt/hoomd-plugin-hpmc/cppmodule
HOOMD_INCLUDE=/home/frankted/hoomd-install/include
CUDA_INCLUDE=/opt/cuda/include
BOOST_INCLUDE=/usr/include
NUMPY_INCLUDE=/usr/lib64/python3.3/site-packages/numpy/core/include/

PYTHON_VERSION = 3.3
PYTHON_INCLUDE = /usr/include/python$(PYTHON_VERSION)
 
# location of the Boost Python include files and library
 
BOOST_INC = /usr/include
BOOST_LIB = /usr/lib
 
# compile mesh classes
CPP11=-std=c++11

all: _rattle.so doc

_rattle.so: rattle_module.o Rattle.o
	g++ -g $(CPP11) -shared -Wl,--export-dynamic /usr/lib64/python$(PYTHON_VERSION)/site-packages/numpy/core/lib/libnpymath.a Rattle.o rattle_module.o -L$(BOOST_LIB) -lboost_python-$(PYTHON_VERSION) -L/usr/lib/python$(PYTHON_VERSION)/config -lpython$(PYTHON_VERSION) -o _rattle.so
	mv _rattle.so py
 
rattle_module.o: rattle_module.cc Rattle.h
	g++ -g $(CPP11) -I$(PYTHON_INCLUDE) -I$(BOOST_INC) -I$(HPMC_INCLUDE) -I$(HOOMD_INCLUDE) -I$(CUDA_INCLUDE) -I$(NUMPY_INCLUDE) -fPIC -c rattle_module.cc

Rattle.o: Rattle.cc Rattle.h
	g++ -g $(CPP11) -I$(PYTHON_INCLUDE) -I$(BOOST_INC) -I$(HPMC_INCLUDE) -I$(HOOMD_INCLUDE) -I$(CUDA_INCLUDE) -I$(NUMPY_INCLUDE) -fPIC -c Rattle.cc

doc: 
	doxygen
	cd latex; make

clean:
	rm -rf *.so *.o py/_*.so py/rattle/__pycache__ html latex

install: 
	@echo "To install, the directory rattle/py must be placed in the PYTHONPATH, and _rattle.so must be in rattle/py/rattle "
