#!/usr/bin/env less

README file for Rattle module:

Rattle is a extension to the HOOMD molecular dynamics package and works in
conjunction with the HPMC extension to determine the free volumes of hard
particles with respect to the contiguous space available to them. The module
includes rendering capabilities, specifically for on-the-fly generation of
POVRay Scene Definition Language (SDL) for a running HOOMD-HPMC simulation. 

Any number of hard particle types may be rattled in the same simulation, so
long as they all inherit from the class hpmc::ShapeConvexPolyhedron. Rendering
of rattle volumes can be performed for all shape classes as well, however the
shapes themselves may not be drawn until they are implemented in the method
Rattle::printSingleShapeSDL().  Currently (as of Feb 25, 2015) only
tetrahedron and truncated_tetrahedron are implemented for drawing.  

The module is written in python and C++ and is imported for use at the
HOOMD/python prompt. BOOST::python bindings are used on the back end. Both
python methods and C++ methods exist in the package. 

The rattle repository contains the following:

hoomd and big_mixture.hoomd examples.
Doxyfile - Doxygen info file for documentation generation.
Makefile - Makefile for building the project.
rattle.hoomd - sample script for using rattle module with approximant.pos.
mixture.hoomd - sample script, mixture of tetrahedra and truncated_tetrahedra.
big_mixture.hoomd - like mixture, but bigger... and with University of
Michigan Cornflower/Blue color scheme for rendering. 
approximant.pos, mixture.pos  - configurations to be used in the examples.
QuaternionMultiply.h - Helper routines for multiplication of quaternions.
Rattle.cc - Source file for the Rattle class.
Rattle.h - Header file for the Rattle class. Contains Doxygen comments also.
rattle_module.cc - boost::python bindings for rattle module.
typed_poly3d_verts.h - Header file used to extend the type hpmc::poly3d_verts
to include a particle type (used for rendering info).
py - directory which contains python module. This directory must be added to
PYTHONPATH in order for the module to be visible to hoomd/python. 
py/rattle - directory containing the python module
__init__.py - declarations of python functions in the rattle python module
23698-poygons-4320p.png - sample image from big_mixture.hoomd

QUICKSTART GUIDE:

- Edit the macros at the *top* of the Makefile for your particular installation of
HOOMD and HPMC.
- run 'make' to build the module in place.
- export PYTHONPATH=`pwd`/py to make the module available to python.
- ./rattle.hoomd to run this example.

DEVELOPER'S NOTE:

I've been learning a lot of C++ on the fly for this project, and am disclosing
a memory leak and its probable most elegant fix. In Rattle.cc, there are
four instances of objects being created with 'new'. Since these objects are
immediately copied into a container, it might make sense that these be declared
as anonymous objects and allowed to fall out of scope gracefully rather than
be destructed explicitly. 
