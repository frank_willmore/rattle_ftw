// typed_poly3d_verts.h

#include <ShapeConvexPolyhedron.h>

/// Derived class of poly3d_verts which includes data member particle_type_id and n_recorded_verts
class typed_poly3d_verts: public hpmc::detail::poly3d_verts {

  public:

  typed_poly3d_verts(char _particle_type_id): particle_type_id(_particle_type_id){} ///<     - constructor that records the particle_type_id for this poly3d_verts object
  char particle_type_id; ///<     - type of particle associated with this poly3d_verts object
  int n_recorded_verts;  ///<     - number of vertices recorded as the type is being spcified vertex by vertex

};

