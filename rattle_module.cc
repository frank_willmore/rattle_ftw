#include <boost/python.hpp>
#include <hoomd/hoomd_config.h>
#include <hoomd/VectorMath.h>
#include "boost/python/extract.hpp"
#include "boost/python/numeric.hpp"
#include <stdio.h>

#include "Rattle.h"

using namespace boost::python;

BOOST_PYTHON_MODULE(_rattle)
{
    //def("getVals", getVals);
    boost::python::numeric::array::set_module_and_type("numpy", "ndarray");
    //class_<Rattle>("Rattle",  init<boost::python::numeric::array>());
    class_<Rattle>("Rattle",  init<boost::python::numeric::array, int, int>())
    .def("addVertex", &Rattle::addVertex)
    .def("addParticleTypeID", &Rattle::addParticleTypeID)
    .def("addRenderType", &Rattle::addRenderType)
    .def("setCamera", &Rattle::setCamera)
    .def("setLookAt", &Rattle::setLookAt)
    .def("setBackgroundColor", &Rattle::setBackgroundColor)
    .def("useFillLight", &Rattle::useFillLight)
    .def("addParticle", &Rattle::addParticle)
    .def("rattleShape", &Rattle::rattleShape)
    .def("setVerletRadius", &Rattle::setVerletRadius)
    .def("setResolution", &Rattle::setResolution)
    .def("openOutputFile", &Rattle::openOutputFile)
    .def("setShapeTexture", &Rattle::setShapeTexture)
    .def("setRattleTexture", &Rattle::setRattleTexture)
    .def("printSingleShapeSDL", &Rattle::printSingleShapeSDL)
    .def("printRattleSDL", &Rattle::printRattleSDL)
    .def("printSDLHeader", &Rattle::printSDLHeader)
    .def("dumpStatistics", &Rattle::dumpStatistics)
    .def("finalizeConfiguration", &Rattle::finalizeConfiguration);
}


