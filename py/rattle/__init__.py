#!/usr/bin/env python

import _rattle
from _rattle import *

################################### ################################### ################################### ###################################

# a function to call that does everything

def create_rattle(system, verts_dict):

  import numpy as np
  import rattle
  positions    = np.array([p.position for p in system.particles], dtype=np.float64)
  orientations = np.array([p.orientation for p in system.particles], dtype=np.float64)
  types        = [p.type for p in system.particles]
  box_dim      = np.array([system.box.Lx,system.box.Ly,system.box.Lz], dtype=np.float64)
  n_particles  = len(positions)
  n_particle_types = len(verts_dict)

  #Create the Rattle object...
  r=rattle.Rattle(box_dim, n_particles, n_particle_types)

  # For each particle_type_id (e.g. 'A', 'B', ...)
  for particle_type_id in verts_dict.keys():
    verts        = verts_dict[particle_type_id]
    np_verts     = np.array(verts, dtype=np.float64)
    n_verts      = len(np_verts)
    r.addParticleTypeID(particle_type_id, n_verts)

    # Add the verts for this particle_type_id
    for i in range(n_verts):
      r.addVertex(particle_type_id, np_verts[i])

  # Add the particles
  for i in range(n_particles):
    r.addParticle(types[i], positions[i], orientations[i])

  r.finalizeConfiguration()
  return r

